// Document elements
// Bank
const bankBalance = document.getElementById("bankBalance");
const loanTitle = document.getElementById("loanTitle");
const loanBalance = document.getElementById("loanBalance");
const getLoanBtn = document.getElementById("getLoanBtn");

// Work
const workBalance = document.getElementById("workBalance");
const bankButtonBtn = document.getElementById("bankButtonBtn");
const workButtonBtn = document.getElementById("workButtonBtn");
const repayButtonBtn = document.getElementById("repayButtonBtn");

// Computer
const computersElement = document.getElementById("computers");
const computerFeatures = document.getElementById("computerFeatures");

const compImg = document.getElementById("compImg");
const computerTitleElement = document.getElementById("computerTitle");
const computerDescElement = document.getElementById("computerDesc");
const computerPriceElement = document.getElementById("computerPrice");
const buyCompBtn = document.getElementById("buyCompBtn");

// Variables
let accountBalance = 0;
let loanAmount = 0;
let totalAmount = 0;
let haveOutstandingLoan = false;
let haveBoughtComputer = true;

let payBalance = 0;

let computers = [];
let currentComputer = undefined;
let url = "https://noroff-komputer-store-api.herokuapp.com/";

// Fetch data
fetch(url + "computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

// Functions
// Adds all the computers from the URL to the drop down menu.
const addComputersToMenu = (computers) => {
  computers.forEach((x) => addComputerToMenu(x));
  compImg.src = url + computers[0].image;
  computerTitleElement.innerText = computers[0].title;
  computerDescElement.innerText = computers[0].description;
  updateBalanceElement(computerPriceElement, computers[0].price);
  setCurrentComputer(computers[0]);
  computers[0].specs.forEach((x) => addFeature(x));
};

// Creates and adds child element with computer title to drop down.
const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
};

// Sets the text for the computer add section.
const handleComputerAdd = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  setCurrentComputer(selectedComputer);
  if (selectedComputer.id == 5) {
    let newUrl = (url + selectedComputer.image).replace("jpg", "png");
    compImg.src = newUrl;
  } else {
    compImg.src = url + selectedComputer.image;
  }
  computerTitleElement.innerText = selectedComputer.title;
  computerDescElement.innerText = selectedComputer.description;
  updateBalanceElement(computerPriceElement, selectedComputer.price);
};

// Fetches the specs for the selected computer.
const handleComputerFeatures = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  let specs = selectedComputer.specs;
  computerFeatures.innerText = "";
  specs.forEach((x) => addFeature(x));
};

// Add the specs to the feature list in laptop section.
const addFeature = (description) => {
  computerFeatures.innerText += description + " \n";
};

// Sets the current computer.
const setCurrentComputer = (computer) => {
  currentComputer = computer;
};

// Updates balance elements on the page.
const updateBalanceElement = (element, number) => {
  element.innerText = number + "kr.";
};

// Toggle visibility on elements based on number.
const toggleVisibility = (element, sum) => {
  if (sum <= 0) {
    element.style.visibility = "hidden";
  } else {
    element.style.visibility = "visible";
  }
};

// Creates a loan.
const getLoan = () => {
  if (!haveOutstandingLoan && haveBoughtComputer) {
    let input = prompt("HOW MUCH LOAN?");
    if (input != null) {
      let sum = parseInt(input);
      if (sum <= accountBalance * 2) {
        haveOutstandingLoan = true;
        haveBoughtComputer = false;
        loanAmount = parseInt(sum);
        accountBalance += loanAmount;
        toggleVisibility(repayButtonBtn, loanAmount);
        toggleVisibility(loanBalance, loanAmount);
        toggleVisibility(loanTitle, loanAmount);
        updateBalanceElement(bankBalance, accountBalance);
        updateBalanceElement(loanBalance, loanAmount);
      } else {
        alert("You can only loan up to x 2 of balance.");
      }
    }
  } else {
    if (haveOutstandingLoan) {
      alert("NEED REPAY FIRST");
    } else if (!haveBoughtComputer) {
      alert("NEED TO BUY COMPUTER FIRST");
    }
  }
};

// Transfers payment to bank account.
const transferPayToBank = () => {
  if (loanAmount > 0) {
    let deduct = payBalance * 0.1;
    let left = payBalance - deduct;
    accountBalance += left;
    loanAmount -= deduct;
    updateBalanceElement(bankBalance, accountBalance);
    updateBalanceElement(loanBalance, loanAmount);
  } else {
    accountBalance += payBalance;
    updateBalanceElement(bankBalance, accountBalance);
  }
  payBalance = 0;
  updateBalanceElement(workBalance, payBalance);
};

// Gives pay for "work".
const work = () => {
  payBalance += 100;
  updateBalanceElement(workBalance, payBalance);
};

// Repays loan.
const repayLoan = () => {
  if (haveOutstandingLoan) {
    if (payBalance >= loanAmount) {
      haveOutstandingLoan = false;
      payBalance -= loanAmount;
      loanAmount = 0;
    } else {
      loanAmount -= payBalance;
      payBalance = 0;
    }
    updateBalanceElement(workBalance, payBalance);
    updateBalanceElement(loanBalance, loanAmount);
    toggleVisibility(repayButtonBtn, loanAmount);
    toggleVisibility(loanBalance, loanAmount);
    toggleVisibility(loanTitle, loanAmount);
  }
};

// Tries to buy computer.
const buyComputer = () => {
  let computerPrice = currentComputer.price;
  if (accountBalance >= computerPrice) {
    haveBoughtComputer = true;
    let reminder = accountBalance - computerPrice;
    accountBalance = reminder;
    updateBalanceElement(bankBalance, accountBalance);
    alert("You bought " + currentComputer.title);
  } else {
    alert("INSUFFICENT FUNDS");
  }
};

// Event Listeners
computersElement.addEventListener("change", handleComputerAdd);
computersElement.addEventListener("change", handleComputerFeatures);
getLoanBtn.addEventListener("click", getLoan);
bankButtonBtn.addEventListener("click", transferPayToBank);
workButtonBtn.addEventListener("click", work);
repayButtonBtn.addEventListener("click", repayLoan);
buyCompBtn.addEventListener("click", buyComputer);
